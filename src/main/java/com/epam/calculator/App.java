package com.epam.calculator;

/**
 * Hello world!
 *
 */
import java.util.Scanner;
import com.epam.calculator.cal.Calci;
public class App 
{
    public static void main( String[] args )
    {
    	Scanner sc = new Scanner(System.in);
        Calci calci = new Calci();

        System.out.print("Enter first operand: ");
        float op1 = sc.nextFloat();
        System.out.print("Enter operator: ");
        char opr = sc.next().charAt(0);
        System.out.print("Enter second operand: ");
        float op2 = sc.nextFloat();

        float result = calci.getResult(opr,op1,op2);

        if(result!=-1)
            System.out.println("Answer: "+result);
        else
            System.out.println("Invalid Operation");
    }
}
